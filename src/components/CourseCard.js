import { useState } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCard ({courseProp}) {

	// console.log(props);
	// console.log(typeof props);

	console.log(courseProp);
	const { name, description, price } = courseProp;

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll () {

		if (seats > 0) {
			setCount(count + 1);
			setSeats(seats - 1);

			console.log('Enrollees' + 1);

		} else {
			alert("No more seats.");
		}
	}

	return (
		<Row className="mb-3">
			<Col>
				<Card>
					<Card.Title className="p-3">{courseProp.name}</Card.Title>

				  	<Card.Body>
				    
				    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
				    <Card.Text>
				    	{courseProp.description}
				    </Card.Text>

				    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
				    <Card.Text>
				    	Php {courseProp.price}
				    </Card.Text>
				    <Card.Text>
				    	Enrollees: {count}
				    </Card.Text>

				    <Button variant="primary" href="#enroll" onClick={enroll}>Enroll</Button>

				  </Card.Body>
				</Card>
			</Col>
		</Row>

	)

}